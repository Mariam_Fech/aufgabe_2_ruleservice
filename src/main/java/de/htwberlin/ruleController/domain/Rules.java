package de.htwberlin.ruleController.domain;

public enum Rules {
	
	pass_on,
	no_pass_on,
	one_deck,
	double_deck

}
