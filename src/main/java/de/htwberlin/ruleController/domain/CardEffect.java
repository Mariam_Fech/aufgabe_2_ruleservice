package de.htwberlin.ruleController.domain;

public enum CardEffect {
	/** open card has no special effect. */
	NO_EFFECT,
	
	/** open card makes next player to skip his turn. */
	SKIP,
	
	/** open card allows the player who placed it to wish a suit. */
	WISH,
	
	/** open card obliges the next player to take 2 cards */
	PLUS_TWO;	
}
