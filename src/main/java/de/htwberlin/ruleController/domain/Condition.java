package de.htwberlin.ruleController.domain;

import de.htwberlin.deckController.domain.Card;

// TODO: Auto-generated Javadoc
/**
 * The Enum Condition.
 */
public enum Condition {

	/** The no effect. */
	NO_EFFECT,
	
	/** The plus two. */
	PLUS_TWO,
	
	/** The plus four. */
	PLUS_FOUR,
	
	/** The plus six. */
	PLUS_SIX,
	
	/** The plus eight. */
	PLUS_EIGHT,
	
	/** The skip. */
	SKIP,
	
	/** The wish hearts. */
	WISH_HEARTS,
	
	/** The wish spades. */
	WISH_SPADES,
	
	/** The wish diamonds. */
	WISH_DIAMONDS,
	
	/** The wish clubs. */
	WISH_CLUBS;
	
}
