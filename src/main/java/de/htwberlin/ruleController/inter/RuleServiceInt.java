package de.htwberlin.ruleController.inter;
import de.htwberlin.ruleController.domain.CardEffect;
import de.htwberlin.ruleController.domain.Condition;
import de.htwberlin.deckController.domain.Card;


public interface RuleServiceInt {
	/**
	 * Gets the card effect.
	 * checks for special rule that may come with the placed card
	 *
	 * @param currentCard the placed open card on open deck
	 * @return the card effect
	 */
	public Condition getCondition(Card currentCard);
	
	/**
	 * Checks if the move chosen by player is allowed.
	 * Compares chosen card to move with the last played card 
	 * and considering any possible special condition
	 *
	 * @param currentCard the card chosen by player to make his next move
	 * @param lastPlayedCard the last played card
	 * @param condition the condition the open deck is in
	 * @return true, if the chosen move is valid
	 */
	public boolean moveAllowed(Card currentCard, Card lastPlayedCard, Condition condition);
	
	/**
	 * Returns Decision according to Number of Players if to double the deck.
	 * If more than two players in the game then the play deck consists of two identic decks
	 * @param countPlayer
	 * @return boolean true = double deck, false = simple deck
	 */
	public boolean doubleDeck(int countPlayer);
		
}
